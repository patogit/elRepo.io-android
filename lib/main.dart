import 'package:elRepoIo/constants.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'retroshare.dart' as rs;
import 'ui.dart' as ui;
import 'utils.dart' as utils;
import 'intl.dart';

void main() async {
  runApp(MaterialApp(

    localizationsDelegates: [
      const SimpleLocalizationsDelegate(),
      GlobalMaterialLocalizations.delegate,
      GlobalWidgetsLocalizations.delegate,
    ],
    supportedLocales: [
      const Locale('en', ''),
      const Locale('es', ''),
    ],

    theme: ThemeData(
      primaryColor: Colors.lightGreen[200],
      accentColor: Colors.purple[200],
    ),
    initialRoute: '/',
    routes: {
      '/': (context) => SplashScreen(),
      '/signup': (context) => ui.SignUpScreen(),
      '/login': (context) => ui.LoginScreen(),
      '/status': (context) => ui.StatusScreen(),
      '/main': (context) => DefaultTabController(
            length: 4,
            child: Scaffold(
              appBar: AppBar(
                title: Text('elRepo.io'),
                elevation: 0.0,
                bottom: TabBar(
                  indicator: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(6),
                        topRight: Radius.circular(6)),
                    color: Colors.white,
                  ),
                  labelColor: NEUTRAL_COLOR,
                  unselectedLabelColor: NEUTRAL_COLOR,
                  tabs: [
                    Tab(text: IntlMsg.of(context).publish, icon: Icon(Icons.add)),
                    Tab(text: IntlMsg.of(context).browse, icon: Icon(Icons.search)),
                    Tab(text: IntlMsg.of(context).tags, icon: Icon(Icons.category)),
                    Tab(text: IntlMsg.of(context).localRepo, icon: Icon(Icons.local_library)),
                  ],
                ),
              ),
              drawer: Drawer(
                child: ListView(
                  // Important: Remove any padding from the ListView.
                  padding: EdgeInsets.zero,
                  children: <Widget>[
                    DrawerHeader(
//                      child: Text(''),
                      decoration: BoxDecoration(
                        color: Colors.purple[400],
                      ),
                    ),
                    ListTile(
                      title: Text('App Status'),
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.pushNamed(context, '/status');
                      },
                    ),
                  ],
                ),
              ),
              body: TabBarView(
                children: [
                  ui.PublishScreen(),
                  ui.BrowseScreen(),
                  ui.TagsScreen(),
                  ui.BookmarksScreen(),
                ],
              ),
            ),
          ),
    },
    onGenerateRoute: (RouteSettings settings) {
      print('build route for ${settings.name} using ${settings.arguments}');
      var routes = <String, WidgetBuilder>{
        "/postdetails": (context) => ui.PostDetailsScreen(settings.arguments),
        "/browsetag": (context) => ui.BrowseTagScreen(settings.arguments),
      };
      WidgetBuilder builder = routes[settings.name];
      return MaterialPageRoute(builder: (context) => builder(context));
    },
  ));
}

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Future<List> postHeaders;

  @override
  void initState() {
    super.initState();
    checkState(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(IntlMsg.of(context).elRepoIoIsLoading),
      ),
    );
  }
}

void checkState(BuildContext context) async {
  utils.createAppDir();

  var isRunning = await rs.isRetroshareRunning();
  if (!isRunning) {
    await rs.startRetroshare();
  }

  if (await rs.RsLoginHelper.isLoggedIn()) {
    print("Is logged in");
    Navigator.pushReplacementNamed(context, '/main');
  } else {
    if (await rs.RsLoginHelper.hasLocation()) {
      print("Has Location -> Login");
      Navigator.pushReplacementNamed(context, '/login');
    } else {
      print("Does Not Have Location -> SignUp");
      Navigator.pushReplacementNamed(context, '/signup');
    }
  }
}
