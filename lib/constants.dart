import 'package:flutter/material.dart';

const APP_DIR = 'net.altermundi.elrepoio';
const API_VERSION = "0.0.9";
const FORUM_PREPEND = "elRepo.io_";
const SUMMARY_LENGTH = 100;
final BACKGROUND_COLOR = Colors.green[50];
final FULL_PAGE_BACKGROUND_COLOR = Colors.white;
const LOCAL_COLOR = Colors.green;
const REMOTE_COLOR = Colors.purple;
const NEUTRAL_COLOR = Colors.black54;

class MarkupTypes {
  static const String plain = "plain";
  static const String markdown = "markdown";
  static const String html = "html";
}

class ContentTypes {
  static const String ref = "ref";
  static const String image = "image";
  static const String audio = "audio";
  static const String video = "video";
  static const String document = "document";
  static const String text = "text";
  static const String file = "file";
}
