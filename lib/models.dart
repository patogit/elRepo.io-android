import 'dart:convert';

class PayloadLink {
  String filename = '';
  String link = '';
  Map<String, dynamic> toJson() => {'filename': filename, 'link': link};

  PayloadLink(this.filename, this.link);
}

class PostBody {
  String text = '';
  List<PayloadLink> payloadLinks = [];

  Map<String, dynamic> toJson() => {'text': text, 'payloadLinks': payloadLinks};
}

class PostMetadata {
  String title;
  String summary;
  String markup;
  String contentType;
  String referred;
  PostMetadata(
      {this.title, this.summary, this.markup, this.contentType, this.referred});

  // get metadata for a linkPost
  PostMetadata.generateLinkPostMetadata(
      PostMetadata postMetadata, String msgId, String forumId)
      : this(
            title: postMetadata.title,
            summary: postMetadata.summary,
            markup: postMetadata.markup,
            contentType: postMetadata.contentType,
            referred:
                "$forumId $msgId"); // reference to the actual content post

  Map<String, dynamic> toJson() => {
        'title': title,
        'summary': summary,
        'markup': markup,
        'contentType': contentType,
        'referred': referred
      };

  factory PostMetadata.fromJsonString(String jsonString) {
    final json = jsonDecode(jsonString);
    return PostMetadata(
        title: json['title'],
        summary: json['summary'],
        markup: json['markup'],
        contentType: json['contentType'],
        referred: json['referred']);
  }
}
