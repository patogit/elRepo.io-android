import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' show SynchronousFuture;

class QueryMap {
  Map _data = new Map();

  QueryMap();

  noSuchMethod(Invocation invocation) {
    if (invocation.isGetter) {
      var ret = _data[invocation.memberName.toString()];
      if (ret != null) {
        return ret;
      } else {
        super.noSuchMethod(invocation);
      }
    }
    if (invocation.isSetter) {
      _data[invocation.memberName.toString().replaceAll('=', '')] =
          invocation.positionalArguments.first;
    } else {
      super.noSuchMethod(invocation);
    }
  }
}

class IntlMsg {
  IntlMsg(this.locale);
  final Locale locale;

  static IntlMsg of(BuildContext context) {
    return Localizations.of<IntlMsg>(context, IntlMsg);
  }

  @override
  noSuchMethod(Invocation invocation) {
    if (invocation.isGetter) {
      RegExp exp = new RegExp(r'Symbol\("(\w+)"\)');
      RegExpMatch match = exp.firstMatch(invocation.memberName.toString());
      if (match.groupCount > 0) {
        String entry = match.group(1);
        print("the method called was: ${match.groupCount}");
        var ret = _localizedValues[locale.languageCode][entry];
        if (ret != null) {
          return ret;
        } else {
          var ret = _localizedValues['en'][entry];
          if (ret != null) {
            return ret;
          } else
            return entry;
        }
      }
    }
    super.noSuchMethod(invocation);
  }

  //TODO(nicoechaniz): find out if it's possible to skip adding these abstract declarations in dart.

  // main.dart
  get elRepoIoIsLoading;
  get publish;
  get browse;
  get tags;
  get localRepo;

  // signup.dart
  get signUp;
  get username;
  get usernameCannotBeEmpty;
  get password;
  get passwordCannotBeEmpty;
  get creatingAccount;
  get createAccount;

  // login.dart
  get loggingIn;
  get loginFailed;
  get login;

  // postdetail.dart
  get cannotLoadContent;
  get contentAlreadyInLocalRepo;
  get savingContentToLocalRepo;

  // publish.dart
  get title;
  get titleCannotBeEmpty;
  get description;
  get chooseFile;
  get publishingContent;
  get contentPublished;
  get checkItOut;

  static Map<String, Map<String, String>> _localizedValues = {
    'en': {
      // main.dart
      'elRepoIoIsLoading': 'elRepo.io is Loading ...',
      'publish': 'publish',
      'browse': 'browse',
      'tags': 'tags',
      'localRepo': 'local\nrepo',

      // signup.dart
      'signUp': 'Sign up',
      'username': 'username',
      'usernameCannotBeEmpty': 'username cannot be empty',
      'password': 'password',
      'passwordCannotBeEmpty': 'password cannot be empty',
      'creatingAccount': "... creating account.\n This may take a while.",
      'createAccount': 'Create account',

      // login.dart
      'loggingIn': 'Logging in. This can take a while...',
      'loginFailed': 'Login failed. Please retry.',
      'login': 'Log in',

      // postdetail.dart
      'cannotLoadContent': 'Cannot load content',
      'contentAlreadyInLocalRepo': 'Content is already in your local Repo',
      'savingContentToLocalRepo': 'Saving content to your local Repo...',

      // publish.dart
      'title': 'title',
      'titleCannotBeEmpty': 'title cannot be empty',
      'description': 'description',
      'chooseFile': 'Choose File',
      'publishingContent':
          'Publishing content...\n If you have added heavy files this may take several minutes to complete.',
      'contentPublished': 'Content published',
      'checkItOut': 'Check it out',
    },

    'es': {
      // main.dart
      'elRepoIoIsLoading': 'elRepo.io está cargando ...',
      'publish': 'publicar',
      'browse': 'buscar',
      'tags': 'tags',
      'localRepo': 'repo\nlocal',

      // signup.dart
      'signUp': 'Crear cuenta',
      'username': 'nombre o apodo',
      'usernameCannotBeEmpty': 'el nombre no puede estar vacío',
      'password': 'clave',
      'passwordCannotBeEmpty': 'la clave no puede estar vacía',
      'creatingAccount': "... creando cuenta.\n Esto puede llevar varios minutos",
      'createAccount': 'Crear cuenta',

      // login.dart
      'loggingIn': 'Ingresando. Esto puede llevar un tiempo...',
      'loginFailed': 'El ingreso falló. Por favor reintnte.',
      'login': 'Ingresar',

      // postdetail.dart
      'cannotLoadContent': 'No se puede cargar el contenido',
      'contentAlreadyInLocalRepo': 'El contenido ya está en tu repo local',
      'savingContentToLocalRepo': 'Guardando contenido a tu repo local...',

      // publish.dart
      'title': 'título',
      'titleCannotBeEmpty': 'el título no puede estar vacío',
      'description': 'descripción',
      'chooseFile': 'Elegir archivo',
      'publishingContent': 'Publicando contenido...\n Si se agregaron archivos pesados el proceso puede demorar varios minutos.',
      'contentPublished': 'Contenido publicado',
      'checkItOut': 'Visitarlo',

    },
  };
}

class SimpleLocalizationsDelegate extends LocalizationsDelegate<IntlMsg> {
  const SimpleLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'es'].contains(locale.languageCode);

  @override
  Future<IntlMsg> load(Locale locale) {
    return SynchronousFuture<IntlMsg>(IntlMsg(locale));
  }

  @override
  bool shouldReload(SimpleLocalizationsDelegate old) => false;
}
