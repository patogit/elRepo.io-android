import 'dart:async';
import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

// In memory local state
String authLocationId;
String authIdentityId;
String authPassphrase;
String authLocationName;

const RETROSHARE_HOST = "127.0.0.1";
const RETROSHARE_PORT = 9092;
const RETROSHARE_SERVICE_PREFIX = "http://$RETROSHARE_HOST:$RETROSHARE_PORT";

const RS_MSG_PENDING = 0x0002;

const RETROSHARE_CHANNEL_NAME = "net.altermundi.elrepoio/retroshare";
const rsPlatform = const MethodChannel(RETROSHARE_CHANNEL_NAME);

class LoginException implements Exception {
  String errorMessage() {
    return 'Please, log in first ...';
  }
}

/* TODO(nicoechaniz): implement when integrating rs service into the app
import 'package:flutter/services.dart';
const rsPlatform = const MethodChannel(RETROSHARE_CHANNEL_NAME);
*/

/// Returns an authentication header to use for RS
String makeAuthHeader(String username, String password) {
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  return basicAuth;
}

// ////////////////////////////////////////////////////////////////////////////
// / SERVICE LIFECYCLE MANAGEMENT
// ////////////////////////////////////////////////////////////////////////////

/// Set internal var
initRetroshare({String locationId, String identityId, String passphrase}) {
  if (!kReleaseMode)
    print("Setting RS vars: $locationId, $identityId, $passphrase");
  if (identityId is String) authIdentityId = identityId;
  if (locationId is String) authLocationId = locationId;
  if (passphrase is String) authPassphrase = passphrase;
}

Future<bool> startRetroshare() async {
  int attempts = 10;
  for (; attempts >= 0; attempts--) {
    print("Starting Retroshare Service. Attempts countdown $attempts");
    try {
      bool isUp = await isRetroshareRunning();
      if (isUp) return true;

      await rsPlatform.invokeMethod('start');

      if (attempts == 0) {
        return false;
      }
      await Future.delayed(Duration(seconds: 5));
    } catch (err) {
      await Future.delayed(Duration(seconds: 5));
    }
  }
}

Future<void> stopRetroshare() async {
  try {
    await rsPlatform.invokeMethod('stop');

    await Future.delayed(Duration(milliseconds: 3000));
    bool isUp = await isRetroshareRunning();
    if (isUp) throw Exception("The service did not stop after a while");
  } catch (err) {
    throw Exception("The service could not be stopped");
  }
}

Future<void> restartRetroshare() async {
  try {
    await rsPlatform.invokeMethod('restart');

    await Future.delayed(Duration(milliseconds: 300));
    bool isUp = await isRetroshareRunning();
    if (!isUp) throw Exception("The service did not restart after a while");
  } catch (err) {
    throw Exception("The service could not be restarted");
  }
}

Future<bool> isRetroshareRunning() async {
  final String reqUrl = "$RETROSHARE_SERVICE_PREFIX";
  try {
    final response = await http.get(reqUrl);
    return response != null && response.statusCode is int;
  } catch (err) {
    return false;
  }
}

// ////////////////////////////////////////////////////////////////////////////
// / RS EVENTS
// ////////////////////////////////////////////////////////////////////////////

// class RsEvents {
//   static Socket socket;
//   static StreamSubscription<Uint8List> streamSubscription;

//   static startListeners() async {
//     if (socket != null && streamSubscription != null) return;

//     try {
//       const PATH = '/rsEvents/registerEventsHandler';
//       socket = await Socket.connect(RETROSHARE_HOST, RETROSHARE_PORT);
//       socket.write("GET $PATH");

//       // LISTEN TO EVENTS
//       streamSubscription = socket.listen(onSocketData,
//           onError: onSocketError, onDone: onSocketDone, cancelOnError: false);
//     } catch (err) {
//       // TODO: HANDLE
//     }
//   }

//   static stopListeners() {
//     if (streamSubscription != null) streamSubscription.cancel();
//     streamSubscription = null;
//     socket = null;
//   }

//   static onSocketData(Uint8List data) {
//     final str = new String.fromCharCodes(data).trim();

//     if (!(str is String)) return; // Drop message
//     final decoded = jsonDecode(str.replaceFirst("data: ", ""));

//     if (decoded["retval"] == true)
//       return; // ignore sync ok event
//     else if (!(decoded["event"] is Map) || !(decoded["event"]["mType"] is int))
//       return; // ignore

//     // WHAT TYPE OF EVENT?
//     switch (decoded["event"]["mType"]) {
//       case RS_CHANNEL_MESSAGE:
//         if (decoded["event"]["mServiceType"] !=
//             RS_CHANNEL_MESSAGE_EVENT_TYPE_CHANNEL)
//           return; // ignore
//         else if (!(decoded["event"]["mMsgs"] is List)) return;

//         handleChannelMessages(decoded["event"]["mMsgs"]);

//         break;
//       case RS_INDIVIDUAL_MESSAGE:
//         RsMsgs.getMessage(decoded["event"]["mChangedMessageIds"]).then((msg) {
//           if (!(msg is Map) ||
//               !(msg["msg"] is String) ||
//               !(msg["rsgxsid_srcId"] is String)) return; // drop

//           final String from = msg["rsgxsid_srcId"] ?? "";
//           final Map body = jsonDecode(msg["msg"]);
//           if (!(body is Map)) return; // drop message

//           handleIndividualMessage(from, body.cast<String, dynamic>());
//         }).catchError((err) => print("ERR: $err"));

//         break;
//     }
//   }

//   static onSocketError(error, StackTrace trace) {
//     print("Stream subscription error. Restarting listeners.\n$error");

//     RsEvents.stopListeners();
//     Future.delayed(Duration(milliseconds: 100)).then((_) {
//       RsEvents.startListeners();
//     });
//   }

//   static onSocketDone() {
//     socket.destroy();
//   }
// }

// ////////////////////////////////////////////////////////////////////////////
// / RAW MESSAGE PASSING
// ////////////////////////////////////////////////////////////////////////////

/// Call the given RetroShare JSON API method with given paramethers, and return
/// results, raise exceptions on errors.
/// Path is expected to contain a leading slash "/" and params is expected to
/// be serializable to JSON.
Future<Map<String, dynamic>> rsApiCall(String path,
    {Map<String, dynamic> params}) async {
  final String reqUrl = "$RETROSHARE_SERVICE_PREFIX$path";
  final basicAuth = makeAuthHeader(authLocationId, authPassphrase);

  if (!(await isRetroshareRunning())) {
    if (!kReleaseMode) print("RS service is down");
    throw Exception("RS service is down");
  }

  try {
    final response = await http.post(reqUrl,
        body: jsonEncode(params ?? {}),
        headers: <String, String>{'Authorization': basicAuth});

    if (response == null) throw Exception("Request failed: " + reqUrl);

    switch (response.statusCode) {
      case 200:
        return jsonDecode(utf8.decode(response.bodyBytes));
      case 401:
        throw Exception("Authentication failed");
      case 404:
        throw Exception("Method not found: " + path);
      default:
        throw Exception("Unhandled statusCode: " +
            response.statusCode.toString() +
            " " +
            reqUrl);
    }
  } catch (err) {
    throw err;
  }
}

// ////////////////////////////////////////////////////////////////////////////
// / SPECIFIC RETROSHARE OPERATIONS
// ////////////////////////////////////////////////////////////////////////////

class RsLoginHelper {
  static Future<bool> hasLocation() {
    return _getLocations().then((locations) {
      if (locations is List && locations.length > 0) return true;
      return false;
    });
  }

  /// returns { mLocationId, ... }
  static Future<Map> getDefaultLocation() {
    return _getLocations().then((locations) {
      if (locations is List &&
          locations.length > 0 &&
          locations[0]["mLocationId"] is String) return locations[0];
      return null;
    });
  }

  /// Creates a Retroshare Account
  /// Returns the location to use the account from now on
  static Future<Map> createLocation(
      String locationName, String password) async {
    final mPath = "/rsLoginHelper/createLocation";
    final mParams = {
      "location": {
        "mLocationName": locationName,
        "mPgpName": locationName,
      },
      "password": password,
      'makeHidden': false,
      'makeAutoTor': false
    };
    final response = await rsApiCall(mPath, params: mParams);

    if (!(response is Map))
      throw FormatException("response is not a Map");
    else if (!response["retval"])
      throw FormatException("retval is not true: " + jsonEncode(response));
    else if (!(response["location"] is Map))
      throw FormatException("location is not a Map");
    else if (!(response["location"]["mLocationId"] is String))
      throw FormatException("mLocationId is not a String");

    return response["location"];
  }

  static Future<int> login(Map location, String password) async {
    authLocationName = location["mLocationName"];
    authLocationId = location["mLocationId"];
    authPassphrase = password;
    if (await isLoggedIn()) {
      authIdentityId = await RsIdentity.getOrCreateIdentity();
      return 0;
    }

    var response = await rsApiCall("/rsLoginHelper/attemptLogin", params: {
      "account": authLocationId,
      "password": password,
    });

    if (!(response is Map)) throw FormatException();
    switch (response["retval"]) {
      case 0:
        authIdentityId = await RsIdentity.getOrCreateIdentity();
        print("location/identity in use: $authLocationId / $authIdentityId");
        return 0;
      case 1: // ERR_ALREADY_RUNNING
        throw Exception("Already running");
      case 2: // ERR_CANT_ACQUIRE_LOCK
        throw Exception("The account is already in use");
      case 3: // ERR_UNKNOWN
        print("Could not decrypt the account data");
        return response["retval"];
      default:
        throw Exception("Could not log in");
    }
  }

  static Future<bool> isLoggedIn() async {
    var response = await rsApiCall("/rsLoginHelper/isLoggedIn");
    if (!(response is Map)) throw FormatException();

    final loggedIn = (response["retval"] == true) || (response["retval"] == 1);
    return (loggedIn && authLocationId != null && authPassphrase != null);
  }

  static Future<List<dynamic>> _getLocations() async {
    var response = await rsApiCall("/rsLoginHelper/getLocations");

    if (!(response is Map)) throw FormatException();

    return response["locations"];
  }
}

class RsIdentity {
  static Future<String /*gxsId*/ > createIdentity(
      String name, String pgpPassword) async {
    if (!(await RsLoginHelper.isLoggedIn())) throw LoginException();

    final mPath = "/rsIdentity/createIdentity";
    final mParams = {
      "name": name,
      "pseudonimous": true,
      "pgpPassword": pgpPassword
    };
    final response = await rsApiCall(mPath, params: mParams);

    if (response["retval"] != true) throw Exception("Error creating identity");
    if (!(response["id"] is String)) throw Exception("Invalid response ID");

    // Store auth state
    return response["id"];
  }

  // Returns the ID of the default identity or creates one if there is none defined
  static Future<String /*gxsId*/ > getOrCreateIdentity() async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    if (authIdentityId is String && authIdentityId.length > 1) {
      return authIdentityId;
    }
    try {
      // Request it
      final mPath = "/rsIdentity/getOwnPseudonimousIds";
      final response = await rsApiCall(mPath);

      if (!(response["ids"] is List))
        throw Exception("Invalid response IDs");

      // no id has been created for this location yet so we create one
      else if (response["ids"].length < 1) {
        print("No identity yet. Creating one.");
        final newIdentityId =
            await RsIdentity.createIdentity(authLocationName, authPassphrase);
        if (!(newIdentityId is String) || newIdentityId.length < 1) {
          throw Exception("Could not create Id");
        } else {
          print("New Id: $newIdentityId");
          return newIdentityId;
        }

        // we have and id, so we return it
      } else {
        print("Existing Ids ${response['ids']}");
        return response["ids"][0];
      }
    } catch (err) {
      throw Exception("Could not get Own Ids. Error: $err");
    }
  }

  static Future<bool> isKnownId(String sslId) async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    try {
      final mPath = "/rsIdentity/isKnownId";
      final mParams = {"id": sslId};
      final response = await rsApiCall(mPath, params: mParams);

      return response["retval"] == true;
    } catch (err) {
      return false;
    }
  }

  static Future<void> requestIdentity(String sslId) async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    try {
      final mPath = "/rsIdentity/requestIdentity";
      final mParams = {"id": sslId};
      final response = await rsApiCall(mPath, params: mParams);

      if (response["retval"] != true) throw Exception();
    } catch (err) {}
  }
}

// ----------------------------------------------------------------------------
// Join the network
// ----------------------------------------------------------------------------

class RsPeers {
  /// get the certificate/invite for current identity
  static Future<String> getRetroshareInvite() async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    final mPath = "/rsPeers/GetRetroshareInvite";
    final response = await rsApiCall(mPath);
    return response['retval'];
  }

  /// get short Invite for given sslId
  static Future<String> getShortInvite(String sslId) async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    final mPath = "/rsPeers/GetShortInvite";
    final mParams = {"sslId": sslId};
    final response = await rsApiCall(mPath, params: mParams);
    if (!response["retval"])
      throw Exception("Could not get short invite for $sslId");
    return response['invite'];
  }

  static Future<bool> addSslOnlyFriend(
      String sslId, String pgpId, Map details) async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    final mPath = "/rsPeers/addSslOnlyFriend";
    final mParams = {"sslId": sslId, "pgpId": pgpId, "details": details};
    final response = await rsApiCall(mPath, params: mParams);

    return response["retval"] == true;
  }

  /// Accepts long invite codes only
  static Future<bool> acceptInvite(String base64Payload) async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    final mPath = "/rsPeers/acceptInvite";
    final mParams = {"invite": base64Payload};
    final response = await rsApiCall(mPath, params: mParams);

    return response["retval"] == true;
  }

  /// Accepts short invite codes only
  static Future<bool> acceptShortInvite(String shortBase64Payload) async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    final details = await RsPeers.parseShortInvite(shortBase64Payload);

    final mPath = "/rsPeers/addSslOnlyFriend";
    final params = {
      "sslId": details["id"],
      "pgpId": details["gpg_id"],
      "details": details
    };
    final response = await rsApiCall(mPath, params: params);

    // if (response["retval"] != true)
    //   throw Exception("The invitation could not be accepted");

    // TODO: Fail on RS if public key is not ready

    return response["retval"] == true;
  }

  static Future<void> connectAttempt(String sslId) async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    final mPath = "/rsPeers/connectAttempt";
    final params = {"sslId": sslId};
    final response = await rsApiCall(mPath, params: params);

    if (response["retval"] != true)
      throw Exception("The connection attempt could not be completed");
  }

  static Future<Map<String, dynamic>> parseShortInvite(
      String shortBase64Payload) async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    var mPath = "/rsPeers/parseShortInvite";
    var params1 = {"invite": shortBase64Payload};
    final response = await rsApiCall(mPath, params: params1);

    if (!(response is Map) ||
        (response["retval"] != true && response["retval"] != 1))
      throw Exception("Could not parse the short invite code");
    else if (!(response["details"] is Map) ||
        !(response["details"]["id"] is String) ||
        !(response["details"]["gpg_id"] is String)) {
      throw Exception("Could not parse the short invite code");
    }

    return response["details"];
  }

  static Future<Map<String, dynamic>> getPeerDetails(String peerId) async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    final mPath = "/rsPeers/getPeerDetails";
    final mParams = {"sslId": peerId};

    final response = await rsApiCall(mPath, params: mParams);

    if (response["retval"] != true)
      throw Exception("The details could not be retrieved");
    else if (!(response["det"] is Map))
      throw Exception("The details are not valid");

    return response["det"];
  }

  static Future<List<String>> getOnlineList() async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    final mPath = "/rsPeers/getOnlineList";
    final response = await rsApiCall(mPath);

    if (response["retval"] != true)
      throw Exception("The list could not be retrieved");
    else if (!(response["sslIds"] is List))
      throw Exception("The list is not valid");

    return response["sslIds"].cast<String>().toList();
  }

  static Future<List<String>> getFriendList() async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    final mPath = "/rsPeers/getFriendList";
    final response = await rsApiCall(mPath);

    if (response["retval"] != true)
      throw Exception("The list could not be retrieved");
    else if (!(response["sslIds"] is List))
      throw Exception("The list is not valid");

    return response["sslIds"].cast<String>().toList();
  }

  static Future<List<dynamic>> getGroupInfoList() async {
//    if (!(await RsLoginHelper.isLoggedIn()))
//      throw Exception("Please, log in first");

    final response = await rsApiCall('/rsPeers/getGroupInfoList');
    if (response["retval"] != true)
      throw Exception("Could not retrieve groups info");
    return response["groupInfoList"];
  }

  static Future<bool> isFriend(String sslId) async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    final mPath = "/rsPeers/isFriend";
    final params = {"sslId": sslId};
    final response = await rsApiCall(mPath, params: params);

    return response["retval"] == true;
  }
}

// ----------------------------------------------------------------------------
// Broadcast Discovery
// ----------------------------------------------------------------------------

class RsBroadcastDiscovery {
  static Future<void> enableMulticastListening() async {
    final response =
        await rsApiCall('/rsBroadcastDiscovery/isMulticastListeningEnabled');
    if (!response["retval"]) {
      await rsApiCall('/rsBroadcastDiscovery/enableMulticastListening');
    }
  }

  static Future<List> getDiscoveredPeers() async {
    var response;
    try {
      response = await rsApiCall('/rsBroadcastDiscovery/getDiscoveredPeers');
    } catch (error) {
      throw (Exception("Error discovering peers. $error"));
    }
    return response["retval"];
  }
}

// ----------------------------------------------------------------------------
// Individual messaging
// ----------------------------------------------------------------------------

class RsMsgs {
  /// Sends a private message (payload) to the node(s) from the list and returns
  /// an array with the delivery ID
  static Future<List<String>> sendMail(
      List<String> to, Map<String, dynamic> payload) async {
    if (!(to is List) || to.length == 0)
      return [];
    else if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    final mPath = "/rsMsgs/sendMail";
    final mParams = {
      "from": authIdentityId,
      "to": to,
      "mailBody": jsonEncode(payload)
    };
    final response = await rsApiCall(mPath, params: mParams);

    if (response["errorMsg"] is String && response["errorMsg"].length > 0)
      throw new Exception(response["errorMsg"]);
    else if (response["retval"] < to.length)
      throw new Exception(
          "The message could not be delivered to all recipients");

    if (!(response["trackingIds"] is List))
      throw Exception("The message could not be delivered");

    List<String> trackingIds = [];
    for (var item in response["trackingIds"] ?? []) {
      if (item["mMailId"] is String) trackingIds.add(item["mMailId"]);
    }

    return trackingIds;
  }

  /// Returns a list of {msgId, srcId, msgflags, msgtags}
  static Future<List<Map<String, dynamic>>> getMessageSummaries() async {
    final response = await rsApiCall('/rsMsgs/getMessageSummaries');

    if (!(response is Map) || !(response["msgList"] is List))
      throw Exception("Could not retrieve the message summaries");
    return response["msgList"].cast<Map<String, dynamic>>().toList() ?? [];
  }

  static Future<Map<String, dynamic>> getMessage(String msgId) async {
    if (!(msgId is String) || msgId.length < 1)
      throw Exception("Invalid msgId");

    final response =
        await rsApiCall('/rsMsgs/getMessage', params: {"msgId": msgId});

    if (!(response is Map) || response["retval"] != true) return null;
    return response["msg"] ?? {};
  }

  static Future<bool> messageDelete(String msgId) async {
    if (!(msgId is String) || msgId.length < 1)
      throw Exception("Invalid msgId");

    final response =
        await rsApiCall('/rsMsgs/MessageDelete', params: {"msgId": msgId});

    if (!(response is Map)) throw Exception("Could not delete");
    return response["retval"] == true;
  }
}

// ----------------------------------------------------------------------------
// Used to obtain a channel ID
// ----------------------------------------------------------------------------

class RsGxsCircle {
  static Future<bool> requestMembership(String circleId) async {
    if (!(circleId is String) || circleId.length < 1)
      throw Exception("Invalid circle ID");

    final response = await rsApiCall('/rsGxsCircles/requestCircleMembership',
        params: {"ownGxsId": authIdentityId, "circleId": circleId});

    if (!(response is Map)) throw Exception("Could not subscribe");
    return response["retval"] == true;
  }

  static Future<Map<String, dynamic>> getCircleDetails(String circleId) async {
    if (!(circleId is String) || circleId.length < 1)
      throw Exception("Invalid circle ID");

    final response = await rsApiCall('/rsGxsCircles/getCircleDetails',
        params: {"id": circleId});

    if (!(response is Map))
      throw Exception("Could not retrieve the details");
    else if (response["retval"] != true)
      throw Exception("The circle details are not available");
    return response["details"];
  }
}

// ----------------------------------------------------------------------------
// Messages broadcasted to channels
// ----------------------------------------------------------------------------

class RsGxsChannel {
  static Future<void> subscribe(String channelId) async {
    final response = await rsApiCall('/rsGxsChannels/subscribeToChannel',
        params: {"channelId": channelId, "subscribe": true});

    if (!(response is Map) || response["retval"] != true)
      throw Exception("Could not subscribe");
  }

  static Future<void> unsubscribe(String channelId) async {
    final response = await rsApiCall('/rsGxsChannels/subscribeToChannel',
        params: {"channelId": channelId, "subscribe": false});

    if (!(response is Map) || response["retval"] != true)
      throw Exception("Could not unsubscribe");
  }

  /// Fetch the list of all contents, along with their ID, status and timestamp.
  /// Returns a map like { mMsgId: "", mmOrigMsgId: "", mMsgFlags: 0x1234, mMsgStatus: 0x1234 }
  static Future<List<Map<String, dynamic>>> getContentSummaries(
      String channelId) async {
    if (!(channelId is String) || channelId.length < 1)
      throw Exception("Invalid channel ID");

    final response = await rsApiCall('/rsGxsChannels/getContentSummaries',
        params: {"channelId": channelId});

    if (!(response is Map) || response["retval"] != true)
      throw Exception("Could not subscribe");
    else if (!(response["summaries"] is List))
      throw new Exception("Invalid summaries");

    return (response["summaries"] as List)
        .cast<Map<String, dynamic>>()
        .toList();
  }

  static Future<List<Map<String, dynamic>>> getChannelsSummaries() async {
    final response = await rsApiCall('/rsGxsChannels/getChannelsSummaries');

    if (!(response is Map) || response["retval"] != true)
      throw Exception("Could not retrieve the summary");
    else if (!(response["channels"] is List))
      throw new Exception("Invalid channels");

    return (response["channels"] as List).cast<Map<String, dynamic>>().toList();
  }

  /// Fetches the given messages from the Channel
  static Future<List<Map<String, dynamic>>> getChannelItems(
      String channelId, List<String> msgIds) async {
    if (!(channelId is String) || channelId.length < 1)
      throw Exception("Invalid channel ID");
    else if (!(msgIds is List)) return getChannelContent(channelId); // all new

    final response = await rsApiCall('/rsGxsChannels/getChannelContent',
        params: {"channelId": channelId, "contentsIds": msgIds});

    if (!(response is Map) || response["retval"] != true)
      throw Exception("Could not subscribe");
    else if (!(response["posts"] is List)) throw new Exception("Invalid posts");

    return (response["posts"] as List).cast<Map<String, dynamic>>().toList();
  }

  /// Fetches the relevant messages from the Channel
  static Future<List<Map<String, dynamic>>> getChannelContent(
      String channelId) async {
    final summaries = await getContentSummaries(channelId);
    final msgIds = summaries
        .map((item) => item["mMsgId"] ?? "")
        .toList()
        .cast<String>()
        .asMap();

    if (!(channelId is String) || channelId.length < 1)
      throw Exception("Invalid channel ID");

    // TODO: filter by relevant stuff only

    final response = await rsApiCall('/rsGxsChannels/getChannelContent',
        params: {"channelId": channelId, "contentsIds": msgIds});

    if (!(response is Map) || response["retval"] != true)
      throw Exception("Could not subscribe");
    else if (!(response["posts"] is List)) throw new Exception("Invalid posts");

    return (response["posts"] as List).cast<Map<String, dynamic>>().toList();
  }

  /// Sends a public message to all the channel, on the given thread ID.
  /// Returns the commentMessageId
  static Future<String> createComment(String channelId, String threadId,
      String identityId, Map<String, dynamic> payload) async {
    if (!(channelId is String) || channelId.length < 1)
      throw Exception("Invalid channel ID");
    else if (!(threadId is String) || threadId.length < 1)
      throw Exception("Invalid thread ID");
    else if (!(payload is Map)) throw Exception("Invalid comment payload");

    final response = await rsApiCall('/rsGxsChannels/createCommentV2', params: {
      "authorId": identityId,
      "channelId": channelId,
      "threadId": threadId,
      "comment": payload,
      "parentId": threadId,
      // "origCommentId": "",
    });

    if (!(response is Map) || response["retval"] != true)
      throw Exception("Could not create the comment");
    else if (response["errorMessage"] is String &&
        response["errorMessage"].length > 0)
      throw new Exception(response["errorMessage"]);
    else if (!(response["commentMessageId"] is String))
      throw new Exception("Invalid commentMessageId");

    return response["commentMessageId"];
  }
}

// ----------------------------------------------------------------------------
// Forums
// ----------------------------------------------------------------------------

class RsGxsForum {
  static Future<String> createForumV2(String name) async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    final response = await rsApiCall('/rsGxsForums/createForumV2',
        params: {"name": name, "circleId": ""});
    if (response["retval"] != true)
      throw Exception("Forum could not be created.");
    return response["forumId"];
  }

  static Future<String> createPost(
      String forumId, String title, String mBody, String authorId,
      [String origPostId = ""]) async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    final response = await rsApiCall('/rsGxsForums/createPost', params: {
      "forumId": forumId,
      "title": title,
      "mBody": mBody,
      "authorId": authorId,
      "origPostId": origPostId
    });
    if (response["retval"] != true)
      throw Exception('Post could not be published. ${response["retval"]}');
    return response["postMsgId"];
  }

  static Future<List<dynamic>> getForumsInfo(List<String> forumIds) async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    final response = await rsApiCall('/rsGxsForums/getForumsInfo',
        params: {"forumIds": forumIds});
    if (response["retval"] != true)
      throw Exception("Could not retrieve forums info");
    return response["forumsInfo"];
  }

  static Future<List<dynamic>> getForumsSummaries() async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    final response = await rsApiCall('/rsGxsForums/getForumsSummaries');
    if (response["retval"] != true)
      throw Exception("Could not retrieve forum summaries");
    return response["forums"];
  }

  static Future<List<dynamic>> getForumMsgMetaData(String forumId) async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    final response = await rsApiCall('/rsGxsForums/getForumMsgMetaData',
        params: {"forumId": forumId});
    if (response["retval"] != true)
      throw Exception("Could not retrieve messages metadata");
    return response["msgMetas"];
  }

  static Future<List<dynamic>> getForumContent(
      String forumId, List<String> msgIds) async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");
    final response = await rsApiCall('/rsGxsForums/getForumContent',
        params: {"forumId": forumId, "msgsIds": msgIds});
    if (response["retval"] != true)
      throw Exception("Could not retrieve messages content");
    return response["msgs"];
  }

  static Future<bool> subscribeToForum(String forumId, bool subscribe) async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    final response = await rsApiCall('/rsGxsForums/subscribeToForum',
        params: {"forumId": forumId, "subscribe": true});
    if (response["retval"] != true)
      throw Exception("Could not subscribe to forum");
    return response["retval"] == true;
  }
}

// ----------------------------------------------------------------------------
// Files
// ----------------------------------------------------------------------------

class RsFiles {
// period defaults to 20 years
  static Future<bool> extraFileHash(String localPath,
      [int period = 630720000, int flags = 0x40]) async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    final response = await rsApiCall('/rsFiles/ExtraFileHash', params: {
      "localpath": localPath,
      "period": {"xstr64": period.toString()},
      "flags": flags
    });
    if (response["retval"] != true)
      throw Exception("File hash process failed.");
    return response["retval"] == true;
  }

  static Future<Map> extraFileStatus(String localPath) async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");
    final response = await rsApiCall('/rsFiles/ExtraFileStatus',
        params: {"localpath": localPath});
    if (response["retval"] != true)
      print("Could not retrieve file status for $localPath");
    return response["info"];
  }

  static Future<String> exportFileLink(
      String fileHash, int fileSize, String fileName) async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    final params = {
      "fileHash": fileHash,
      "fileSize": fileSize,
      "fileName": fileName
    };

    final response = await rsApiCall('/rsFiles/exportFileLink', params: params);
    if (response["retval"]["errorNumber"] != 0)
      throw Exception("Could not export file link. $params");

    return response["link"];
  }

  static Future<Map> parseFilesLink(String link) async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    final response =
        await rsApiCall('/rsFiles/parseFilesLink', params: {"link": link});
    if (response["retval"]["errorNumber"] != 0)
      throw Exception("Could not parse file link: $link");

    return response["collection"];
  }

  static Future<void> requestFiles(Map collection) async {
    print("Requesting $collection");
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    final response = await rsApiCall('/rsFiles/requestFiles',
        params: {"collection": collection});
    if (response["retval"]["errorNumber"] != 0)
      throw Exception("Files request failed.");
  }

  static void setDownloadDirectory(String path) async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    final response = await rsApiCall('/rsFiles/setDownloadDirectory',
        params: {"path": path});
    if (response["retval"] != true)
      throw Exception("Error setting download directory.");
  }

  static void setPartialsDirectory(String path) async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    final response = await rsApiCall('/rsFiles/setPartialsDirectory',
        params: {"path": path});
    if (response["retval"] != true)
      throw Exception("Error setting partial download directory.");
  }

  static void addSharedRepoDirectory(String path) async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    final response = await rsApiCall('/rsFiles/addSharedDirectory', params: {
      "dir": {"filename": path, "virtualname": "elRepo.io", "shareflags": 0x80}
    });

    if (response["retval"] != true)
      throw Exception("Error adding shared directory.");
  }

  static Future<List> fileDownloads() async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    final response = await rsApiCall('/rsFiles/FileDownloads');
    return response["hashs"];
  }

  static Future<Map> requestDirDetails([int handle]) async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");
    Map response;
    if (handle == null) {
      response = await rsApiCall('/rsFiles/requestDirDetails');
    } else {
      response = await rsApiCall('/rsFiles/requestDirDetails', params: {
        "handle": handle
//        "handle": {"xstr64": handle}
      });
    }
    if (response["retval"] != true)
      throw Exception("Error requesting Dir Details.");
    return response["details"];
  }

  static Future<Map> fileDetails(String hash, [int hintFlags = 0x04]) async {
    if (!(await RsLoginHelper.isLoggedIn()))
      throw Exception("Please, log in first");

    final response = await rsApiCall('/rsFiles/FileDetails',
        params: {"hash": hash, "hintFlags": hintFlags});
    print("Response: $response");
    if (response["retval"] != true) print("File with hash $hash not found.");
    return response["info"];
  }
}

// UTILITIES

Future<String> waitForFileLink(filePath) async {
  int secsToTimeout = 600;
  var fileInfo;
  for (; secsToTimeout >= 0; secsToTimeout--) {
    try {
      fileInfo = await RsFiles.extraFileStatus(filePath);
    } catch (error) {
      fileInfo = null;
    }

    if (fileInfo == null) {
      await Future.delayed(Duration(seconds: 1));
      continue;
      // for some reason extraFileStatus at some point returns bad fileInfo with empty data
    } else if (fileInfo["fname"] != "" && fileInfo["fname"] != null) {
      var fileLink = await RsFiles.exportFileLink(
          fileInfo["hash"], fileInfo["size"]["xint64"], fileInfo["fname"]);
      return fileLink;
    }
  }
  // timed out
  return "";
}

Future<bool> waitUntilOnline(String sslId, [int attempts = 10]) async {
  // wait a bit until the peer accepts us
  for (; attempts >= 0; attempts--) {
    try {
      var ids = await RsPeers.getOnlineList();
      if ((ids is List) && ids.contains(sslId)) return true; // done

      // else => no news yet => retry
      await RsPeers.connectAttempt(sslId).catchError((_) {});

      if (attempts == 0) {
        // do not wait on the last iteration
        return false;
      }

      // else => retry
      await Future.delayed(Duration(seconds: 5));
    } catch (err) {
      await Future.delayed(Duration(seconds: 5));
    }
  }
  return false;
}

Future<bool> waitUntilSent(String msgId, [int attempts = 5]) async {
  try {
    for (; attempts >= 0; attempts--) {
      final response = await RsMsgs.getMessage(msgId);
      if (response == null)
        return false;
      else if (!(response is Map) ||
          (response["msgflags"] & RS_MSG_PENDING) != 0) {
        // still pending
        await Future.delayed(Duration(seconds: 5));
        continue;
      }
      return true;
    }
    return false;
  } catch (err) {
    return false;
  }
}

Future<dynamic> befriendTier1({String hostname = "michelangiolillo"}) async {
  print("Befriending $hostname");
  String reqUrl = "http://$hostname.elrepo.io/rsPeers/GetRetroshareInvite";
  try {
    final response = await http.get(reqUrl);
    Map decoded;
    if (response.statusCode == 200) {
      decoded = jsonDecode(response.body);
    } else {
      print(
          "Error receiving Tier1 invite. Status code: ${response.statusCode}");
      return;
    }
    String tier1Cert = decoded['retval'];
    RsPeers.acceptInvite(tier1Cert);
  } catch (error) {
    print("Cannot reach $hostname. Error: $error");
    return;
  }

  final myInvite = await RsPeers.getRetroshareInvite();
  reqUrl = "http://$hostname.elrepo.io/rsPeers/acceptInvite";
  Map jsonCert = {"invite": myInvite};

  try {
    final response = await http.post(reqUrl, body: jsonEncode(jsonCert));
    if (response.statusCode != 200)
      print("Error sending cert to $hostname. Code ${response.statusCode}");
    return response;
  } catch (error) {
    print("Connection to $hostname failed. Error: $error ");
  }
}

Future<void> addFriends(List peers) async {
  Map peer;
  for (peer in peers) {
    print("Adding Friend: $peer");
    final sslId = peer["mSslId"];
    final alreadyFriend = await RsPeers.isFriend(sslId);
    if (!alreadyFriend) {
      final pgpFingerprint = peer["mPgpFingerprint"];
      final pgpId = pgpFingerprint.substring(pgpFingerprint.length - 16);
      final uri = peer["mLocator"]["urlString"];
      final parsedUri = Uri.parse(uri);
      final details = {
        "localAddr": parsedUri.host,
        "localPort": parsedUri.port
      };
      RsPeers.addSslOnlyFriend(sslId, pgpId, details);
    }
  }
}
