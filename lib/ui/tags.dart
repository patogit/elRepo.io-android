part of ui;

Future<List> getTagNames() async {
  print("Getting tag names");
  final allForums = await rs.RsGxsForum.getForumsSummaries();
  final tagForums = allForums
      .where((i) => i["mGroupName"]
          .toString()
          .startsWith("$FORUM_PREPEND${API_VERSION}_TAG_"))
      .toList();
  print("Tag forums $tagForums");

  Map forum;
  var allTags = new Set();
  String tagName;
  String groupName;
  for (forum in tagForums) {
    if (forum["mSubscribeFlags"] == 8)
      await rs.RsGxsForum.subscribeToForum(forum["mGroupId"], true);
    groupName = forum["mGroupName"].toString();
    tagName =
        groupName.substring(groupName.lastIndexOf("_TAG_") + '_TAG_'.length);
    allTags.add(tagName);
  }
  print("Found tags: $allTags");
  return (allTags.toList());
}

class TagsScreen extends StatefulWidget {
  @override
  _TagsScreenState createState() => _TagsScreenState();
}

class _TagsScreenState extends State<TagsScreen> {
  Future<List> tagNames;

  @override
  void initState() {
    tagNames = getTagNames();
    super.initState();
  }

  Widget tagsWidget() {
    return FutureBuilder(
      future: tagNames,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          print('Error: ${snapshot.error}');
          return Center(
            child: Text(IntlMsg.of(context).cannotLoadContent),
          );
        } else if (snapshot.hasData == false || snapshot.data.length == 0) {
          return loadingBox();
        } else {
          return ListView.builder(
            itemCount: snapshot.data.length,
            itemBuilder: (context, index) {
              return ListTile(
                title: InkWell(
                    onTap: () {
                      print("tapped on ${snapshot.data[index]}");
                      Navigator.pushNamed(context, "/browsetag",
                          arguments: TagArguments(snapshot.data[index]));
                    },
                    child: Text(snapshot.data[index])),
              );
            },
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green[50],
      body: tagsWidget(),
    );
  }
}
