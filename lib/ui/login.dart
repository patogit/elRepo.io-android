part of ui;

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: FULL_PAGE_BACKGROUND_COLOR,
      appBar: AppBar(
        title: Text("elRepo.io"),
      ),
      body: new Container(
        margin: defaultMargin(),
        child: new Column(children: <Widget>[new LoginForm()]),
      ),
    );
  }
}

class LoginForm extends StatefulWidget {
  LoginForm({Key key}) : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    String password;
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            decoration: InputDecoration(labelText: IntlMsg.of(context).password),
            validator: (value) {
              if (value.isEmpty) {
                return IntlMsg.of(context).passwordCannotBeEmpty;
              }
              return null;
            },
            onSaved: (val) => password = val,
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 5.0),
              child: RaisedButton(
                onPressed: () async {
                  final form = _formKey.currentState;
                  showLoadingDialog(
                      context, IntlMsg.of(context).loggingIn);
                  if (form.validate()) {
                    form.save();
                    // Process data.
                    final currentLocation =
                        await rs.RsLoginHelper.getDefaultLocation();
                    print('logging in $currentLocation');

                    final responseStatus =
                        await rs.RsLoginHelper.login(currentLocation, password);
                    if (responseStatus != 0) {
                      final snackBar = SnackBar(
                        content: Text(IntlMsg.of(context).loginFailed),
                        duration: Duration(seconds: 6),
                      );
                      Navigator.pop(context); //pop dialog
                      Scaffold.of(context).showSnackBar(snackBar);
                      print("Bad login attempt.");
                      return;
                    }

                    print("logged In");

                    //TODO(nicoechaniz): check if already friend
                    rs.befriendTier1(hostname: "emiliocanzi");
                    rs.befriendTier1(hostname: "micheleangiolillo");

                    //TODO(nicoechaniz): check if it's already set
                    final downloadsDir =
                        await ExtStorage.getExternalStoragePublicDirectory(
                            ExtStorage.DIRECTORY_DOWNLOADS);
                    final appDir = '$downloadsDir/$APP_DIR';
                    print(
                        "Setting Retroshare download and shared directory to: $appDir");
                    rs.RsFiles.setDownloadDirectory(appDir);
                    rs.RsFiles.addSharedRepoDirectory(appDir);

                    Navigator.pop(context); //pop dialog
                    Navigator.pushReplacementNamed(context, '/main');
                  }
                },
                child: Text(IntlMsg.of(context).login),
                color: Colors.purple[600],
                textColor: Colors.white,
              ),
            ),
          )
        ],
      ),
    );
  }
}
