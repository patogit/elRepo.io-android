part of ui;

enum RetroshareStatus { SERVICE_OFFLINE, SERVICE_ONLINE, ERROR }
enum LoginStatus { LOGGEDIN, NOT_LOGGEDIN }

class StatusScreen extends StatefulWidget {
  @override
  _StatusScreenState createState() => _StatusScreenState();
}

class _StatusScreenState extends State<StatusScreen>
    with WidgetsBindingObserver, AutomaticKeepAliveClientMixin {
  Timer retroshareStatusTimer;
  RetroshareStatus retroshareStatus;
  LoginStatus loginStatus;
  String retroshareStatusMessage;
  String loginStatusMessage;
  String onlinePeersMessage;
  String timerTick;
  String extraStatusMessages;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    retroshareStatus = RetroshareStatus.SERVICE_OFFLINE;
    loginStatus = LoginStatus.NOT_LOGGEDIN;
    setRetroshareStatusMessage();
    setLoginStatusMessage();
    retroshareStatusTimer = Timer.periodic(
      Duration(seconds: 5),
      retroshareTimerHandler,
    );
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    retroshareStatusTimer.cancel();
    print("called dispose");
    super.dispose();
  }

  void setRetroshareStatusMessage() {
    switch (retroshareStatus) {
      case RetroshareStatus.SERVICE_ONLINE:
        {
          retroshareStatusMessage = "Retroshare online";
          break;
        }
      default:
        retroshareStatusMessage = "Retroshare offline";
    }
  }

  void setLoginStatusMessage() {
    switch (loginStatus) {
      case LoginStatus.LOGGEDIN:
        {
          loginStatusMessage = "You are logged in";
          break;
        }
      default:
        loginStatusMessage = "You are NOT logged in";
    }
  }

  setExtraStatusMessage() async {
    List messageList = [];

    final dirDetails = await rs.RsFiles.requestDirDetails();
    for (var dir in dirDetails['children']) {
      if (dir["name"] == "[Extra List]") {
        final extraDirHandle = dir["handle"];
        final extraDirDetails =
            await rs.RsFiles.requestDirDetails(extraDirHandle);
        messageList.addAll(
            ["EXTRA FILES (pseudo dir) DETAILS:", extraDirDetails.toString()]);
        messageList.add("FILES SHARED:");
        for (var file in extraDirDetails["children"]) {
          final fileHandle = file["handle"];
          final fileName = file["name"];
          final fileDetails = await rs.RsFiles.requestDirDetails(fileHandle);
          final fileStatus =
              await rs.RsFiles.extraFileStatus(fileDetails["path"]);
          messageList.addAll([
            "DETAILS for $fileName:",
            fileDetails.toString(),
            "SATUS for $fileName:",
            fileStatus.toString()
          ]);
        }
      }
    }

    final hashs = await rs.RsFiles.fileDownloads();
    if (hashs.length > 0) {
      messageList.add("DOWNLOADS:");
      for (var hash in hashs) {
        var downloadDetails = await rs.RsFiles.fileDetails(hash);
        messageList.add(downloadDetails);
      }
    }
    extraStatusMessages = messageList.join("\n");
  }

  retroshareTimerHandler(Timer timer) async {
    print("timer running... handler called");
    bool rsRunning = false;
    bool isLoggedIn = false;
    try {
      rsRunning = await rs.isRetroshareRunning();
      isLoggedIn = await rs.RsLoginHelper.isLoggedIn();
    } catch (error) {
      print("Error... back to login");
      retroshareStatusTimer.cancel();
      dispose();
      Navigator.pushReplacementNamed(context, '/login');
    }
    if (!rsRunning || !isLoggedIn) {
      print("RS died or lost logged in status...");
      retroshareStatusTimer.cancel();
      dispose();
      Navigator.pushReplacementNamed(context, '/login');
    }
    List onlinePeers = [];
    if (rsRunning) {
      retroshareStatus = RetroshareStatus.SERVICE_ONLINE;
      if (isLoggedIn) {
        loginStatus = LoginStatus.LOGGEDIN;
        final peers = await rs.RsBroadcastDiscovery.getDiscoveredPeers();
        if (peers.length > 0) {
          print("Peers found: $peers");
          rs.addFriends(peers);
        }

//        List friends = await rs.RsPeers.getFriendList();
        List online = await rs.RsPeers.getOnlineList();

        for (var peer in online) {
          var peerDetails = await rs.RsPeers.getPeerDetails(peer);
          onlinePeers.add(
              '  $peer:\n   ${peerDetails["name"]} / ${peerDetails["location"]}\n');
        }
      } else
        loginStatus = LoginStatus.NOT_LOGGEDIN;
    } else {
      retroshareStatus = RetroshareStatus.SERVICE_OFFLINE;
    }
    if (this.mounted) {
      setState(() {
        setRetroshareStatusMessage();
        setLoginStatusMessage();
        timerTick = timer.tick.toString();
        onlinePeersMessage = 'online friends:\n ${onlinePeers.join()}';
        setExtraStatusMessage();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: Colors.green[50],
      appBar: AppBar(
        title: Text("elRepo.io"),
      ),
      body: new Container(
        margin: defaultMargin(),
        child: new ListView(
          children: <Widget>[
            new Text(
                "$retroshareStatusMessage (Tick: $timerTick)\n$loginStatusMessage\n$onlinePeersMessage"),
            RaisedButton(
              onPressed: () async {},
              child: Text('Test'),
            ),
            new Text("Extra Status Messages: \n $extraStatusMessages"),

/*
            RaisedButton(
              onPressed: () async {
              },
              child: Text('test'),
            )
*/
          ],
        ),
      ),
    );
  }
}
