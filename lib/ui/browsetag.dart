part of ui;

class BrowseTagScreen extends StatefulWidget {
  final TagArguments args;
  BrowseTagScreen(this.args);

  @override
  _BrowseTagScreenState createState() => _BrowseTagScreenState();
}

class _BrowseTagScreenState extends State<BrowseTagScreen> {
  Future<List> postHeaders;

  @override
  void initState() {
    postHeaders = utils.getPostHeaders("TAG", widget.args.tagName);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green[50],
      appBar: AppBar(
        title: Text("elRepo.io"),
      ),
      body: postTeasersWidget(postHeaders, linkPosts: true),
    );
  }
}
