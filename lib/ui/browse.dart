part of ui;

class BrowseScreen extends StatefulWidget {
  @override
  _BrowseScreenState createState() => _BrowseScreenState();
}

class _BrowseScreenState extends State<BrowseScreen> {
  Future<List> postHeaders;
  Future<List> bookmarkedIds;

  @override
  void initState() {
    postHeaders = utils.getPostHeaders();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: postTeasersWidget(postHeaders),
    );
  }
}
