part of ui;

class PublishScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: new Container(
        margin: defaultMargin(),
        child: new ListView(children: <Widget>[new PublishForm()]),
      ),
    );
  }
}

class _PostData {
  models.PostMetadata metadata = new models.PostMetadata(
    markup: MarkupTypes.plain,
    contentType: ContentTypes.text,
  );
  models.PostBody mBody = new models.PostBody();
  String filePath = '';
  String filename = '';
}

class PublishForm extends StatefulWidget {
  PublishForm({Key key}) : super(key: key);

  @override
  _PublishFormState createState() => _PublishFormState();
}

class _PublishFormState extends State<PublishForm> {
  final _formKey = GlobalKey<FormState>();
  _PostData _data = new _PostData();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            decoration: InputDecoration(labelText: IntlMsg.of(context).title),
            validator: (value) {
              if (value.isEmpty) return IntlMsg.of(context).titleCannotBeEmpty;
              _data.metadata.title = value;
              return null;
            },
          ),
          TextFormField(
            maxLines: 3,
            decoration: InputDecoration(
                alignLabelWithHint: true, labelText: IntlMsg.of(context).description),
            validator: (value) {
              _data.mBody.text = value;
              return null;
            },
          ),
          Align(
              alignment: Alignment.bottomLeft,
              child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                  child: RaisedButton(
                    onPressed: () async {
                      _data.filePath = await FilePicker.getFilePath();
                      _data.filename = path.basename(_data.filePath);
                    },
                    child: Text(IntlMsg.of(context).chooseFile),
                    color: Colors.purple[400],
                    textColor: Colors.white,
                  ))),
          Align(
            alignment: Alignment.bottomRight,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 5.0),
              child: RaisedButton(
                onPressed: () async {
                  FocusScopeNode currentFocus = FocusScope.of(context);
                  if (!currentFocus.hasPrimaryFocus) {
                    currentFocus.unfocus();
                  }
                  final form = _formKey.currentState;
                  if (form.validate()) {
                    form.save();
                    print("Post form received file path: ${_data.filePath}");
                    showLoadingDialog(context,
                        IntlMsg.of(context).publishingContent);

                    String msgBody = "";
                    String metadata = "";
                    String fileLink = "";
                    if (_data.filePath != "") {
                      final hashingFile =
                          await rs.RsFiles.extraFileHash(_data.filePath);

                      if (hashingFile == true) {
                        print("Hashing file ${_data.filePath}");
                        fileLink = await rs.waitForFileLink(_data.filePath);
                        _data.mBody.payloadLinks
                            .add(models.PayloadLink(_data.filename, fileLink));
                        print("Hashing done for ${_data.filePath}");
                        _data.metadata.contentType =
                            getContentTypeFromPath(_data.filePath);
                      }
                    }
                    msgBody = jsonEncode(_data.mBody);
                    if (_data.metadata.summary == null) {
                      if (_data.mBody.text.length > SUMMARY_LENGTH) {
                        final summary =
                            _data.mBody.text.substring(0, SUMMARY_LENGTH);
                        _data.metadata.summary = "$summary...";
                      } else {
                        _data.metadata.summary = _data.mBody.text;
                      }
                    }

                    metadata = jsonEncode(_data.metadata);

                    final forumId = await utils.findOrCreateRepoForum();
                    final msgId = await rs.RsGxsForum.createPost(
                        forumId, metadata, msgBody, rs.authIdentityId);

                    final hashTags = utils.findHashTags(_data.mBody.text);
                    final linkPostMetadata = jsonEncode(
                        models.PostMetadata.generateLinkPostMetadata(
                            _data.metadata, msgId, forumId));
                    String tag;
                    for (tag in hashTags) {
                      // we add the reference to the real post

                      final tagForumId =
                          await utils.findOrCreateRepoForum("TAG", tag);
                      print("Creating post in $tagForumId");
                      // we don't save the message body in a linkPost
                      rs.RsGxsForum.createPost(
                          tagForumId, linkPostMetadata, "", rs.authIdentityId);
                    }
                    // we also create a linkPost in the user forum to keep track of content published.
                    final userForumId = await utils.findOrCreateRepoForum(
                        "USER", rs.authIdentityId);
                    rs.RsGxsForum.createPost(
                        userForumId, linkPostMetadata, "", rs.authIdentityId);

                    form.reset();
                    Navigator.pop(context); //pop dialog
                    final snackBar = SnackBar(
                      content: Text(IntlMsg.of(context).contentPublished),
                      duration: Duration(seconds: 6),
                      action: SnackBarAction(
                        label: IntlMsg.of(context).checkItOut,
                        onPressed: () {
                          Navigator.pushNamed(context, "/postdetails",
                              arguments: PostArguments(forumId, msgId, false));
                        },
                      ),
                    );
                    Scaffold.of(context).showSnackBar(snackBar);
                    print("post Published with id: $msgId and tags: $hashTags");
                  }
                },
                child: Text(IntlMsg.of(context).publish),
                color: Colors.purple[600],
                textColor: Colors.white,
              ),
            ),
          )
        ],
      ),
    );
  }
}
