part of ui;

class BookmarksScreen extends StatefulWidget {
  @override
  _BookmarksScreenState createState() => _BookmarksScreenState();
}

class _BookmarksScreenState extends State<BookmarksScreen> {
  Future<List> postHeaders;

  @override
  void initState() {
    postHeaders = utils.getPostHeaders("BOOKMARKS", rs.authIdentityId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: BACKGROUND_COLOR,
      body: postTeasersWidget(postHeaders, linkPosts: true),
    );
  }
}
