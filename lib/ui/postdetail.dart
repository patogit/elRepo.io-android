part of ui;

class PostDetailsScreen extends StatefulWidget {
  final PostArguments args;
  PostDetailsScreen(this.args);

  @override
  _PostDetailsScreenState createState() => _PostDetailsScreenState();
}

class _PostDetailsScreenState extends State<PostDetailsScreen> {
  Future<List> postDetails;

  @override
  void initState() {
    postDetails = getPostDetails(
        widget.args.forumId, widget.args.postId, widget.args.isLinkPost);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: FULL_PAGE_BACKGROUND_COLOR,
      appBar: AppBar(
        title: Text("elRepo.io"),
      ),
      body: postDetailsWidget(),
    );
  }

  Widget postDetailsWidget() {
    models.PostBody postBody = new models.PostBody();
    models.PostMetadata postMetadata;
    String postId;
    IconData contentTypeIcon;

    return FutureBuilder(
      future: postDetails,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          print('Error: ${snapshot.error}');
          return Center(
            child: Text(IntlMsg.of(context).cannotLoadContent),
          );
        } else if (snapshot.hasData == false || snapshot.data.length == 0) {
          return loadingBox();
        } else {
          final data = snapshot.data[0];
          postId = data["mMeta"]["mMsgId"];
          try {
            postMetadata =
                models.PostMetadata.fromJsonString(data["mMeta"]["mMsgName"]);
          } catch (error) {
            print("Error parsing metadata Json: $error");
          }
          try {
            Map jsonBody = jsonDecode(data["mMsg"]);
            postBody.text = jsonBody["text"];
            if (jsonBody["payloadLinks"].length > 0) {
              Map linkData;
              for (linkData in jsonBody["payloadLinks"]) {
                postBody.payloadLinks.add(
                    models.PayloadLink(linkData["filename"], linkData["link"]));
              }
            }
          } catch (error) {
            print("Error parsing body Json: $error");
            // this may be a post originated from another client
            // we are choosing to show it but we could ignore it.
            postBody.text = data["mMsg"];
            postBody.payloadLinks = [];
          }
          if (postBody.payloadLinks.length > 0) {
            final filename = postBody.payloadLinks[0].filename;
            contentTypeIcon = getContentTypeIcon(postMetadata.contentType);
          } else {
            // if there's no associated file, we consider the payload to be the inline text
            contentTypeIcon = Icons.format_align_left;
          }

          return new Container(
              margin: defaultMargin(),
              child: new ListView(children: <Widget>[
                new ListTile(
                  leading: Container(
                      padding: EdgeInsets.only(right: 5.0),
                      decoration: new BoxDecoration(),
                      child: Icon(contentTypeIcon,
                          color: NEUTRAL_COLOR, size: 48)),
                  title: Text(postMetadata.title,
                      style: Theme.of(context).textTheme.subhead),
                  subtitle: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        new IconButton(
                          icon: new Icon(Icons.share, color: NEUTRAL_COLOR),
                          onPressed: () {},
                        ),
                        new PayloadLinkWidget(postMetadata, postId, postBody,
                            widget.args.forumId),
                      ]),
                ),
                Container(
                    padding: defaultMargin(),
                    decoration: new BoxDecoration(
                        border: new Border(
                            top: new BorderSide(
                                width: 1.0, color: Colors.black12))),
                    child: Text(
                      postBody.text,
                      style: Theme.of(context).textTheme.body1,
                    )),
              ]));
        }
      },
    );
  }
}

class PayloadLinkWidget extends StatefulWidget {
  final models.PostBody postBody;
  final models.PostMetadata postMetadata;
  final String postId;
  final String forumId;
  PayloadLinkWidget(
      this.postMetadata, this.postId, this.postBody, this.forumId);

  @override
  _PayloadLinkWidgetState createState() => new _PayloadLinkWidgetState();
}

class _PayloadLinkWidgetState extends State<PayloadLinkWidget> {
  Widget payloadActionIcon;
  Color statusColor;
  String link;
  Map linksData;
  Future<String> filePath;

  @override
  void initState() {
//    statusColor = REMOTE_COLOR;
    filePath = getPathIfExists();
    super.initState();
  }

  Future<String> getPathIfExists() async {
    print("Checking if file exists");
    final postBody = widget.postBody;
    link = postBody.payloadLinks[0].link;
    linksData = await rs.RsFiles.parseFilesLink(link);
    final fileName = linksData['mFiles'][0]["name"];
    final downloadsDir = await ExtStorage.getExternalStoragePublicDirectory(
        ExtStorage.DIRECTORY_DOWNLOADS);
    final appDir = '$downloadsDir/$APP_DIR';
    final path = '$appDir/$fileName';

    if (await Io.File(path).exists()) {
      print("File exists: $path");
      return path;
    } else
      return null;
  }

  @override
  Widget build(BuildContext context) {
    final postMetadata = widget.postMetadata;
    final postId = widget.postId;
    final forumId = widget.forumId;
    payloadActionIcon = getActionIcon(filePath);

    return new IconButton(
        icon:
            payloadActionIcon, //new Icon(payloadActionIcon, color: statusColor),
        onPressed: () {
          filePath.then((path) async {
            if (path != null) {
              print("Opening file");
              final openStatus = await OpenFile.open(path);
              print("Open file result: ${openStatus.message}");
            } else {
              print("Requesting file: $linksData");
              rs.RsFiles.requestFiles(linksData);
              //TODO(nicoechaniz): verify that the file is effectively requested for Download before creating the bookmark post.
              String snackBarText;
              final bookmarksForumId = await utils.findOrCreateRepoForum(
                  "BOOKMARKS", rs.authIdentityId);
              print("Checking for bookmark in forum: $bookmarksForumId");
              final referred = "$forumId $postId";
              final bookmarkHeaders =
                  await utils.getPostHeaders("BOOKMARKS", rs.authIdentityId);
              // TODO(nicoechaniz): find a more efficient way to do this
              List duplicates = bookmarkHeaders
                  .where(
                      (i) => jsonDecode(i["mMsgName"])["referred"] == referred)
                  .toList();
              if (duplicates.length > 0) {
                snackBarText = IntlMsg.of(context).contentAlreadyInLocalRepo;
              } else {
                final linkPostMetadata =
                    models.PostMetadata.generateLinkPostMetadata(
                        postMetadata, postId, forumId);
                rs.RsGxsForum.createPost(
                    bookmarksForumId,
                    json.encode(linkPostMetadata.toJson()),
                    "",
                    rs.authIdentityId);
                snackBarText = IntlMsg.of(context).savingContentToLocalRepo;
              }
              final snackBar = SnackBar(
                content: Text(snackBarText),
                duration: Duration(seconds: 5),
                onVisible: () => {},
              );
              Scaffold.of(context).showSnackBar(snackBar);
              setState(() {
                // TODO(nicoechaniz): This is not working
                payloadActionIcon = Icon(Icons.open_in_new, color: LOCAL_COLOR);
//                statusColor = LOCAL_COLOR;
                print("setting state");
              });
            }
          });
        });
  }

  Widget getActionIcon(Future<String> filePath) {
    Icon postIcon;
    return FutureBuilder(
      future: filePath,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          postIcon = Icon(Icons.error, color: NEUTRAL_COLOR);
          print('Error: ${snapshot.error}');
        } else if (snapshot.hasData == false) {
          return Icon(Icons.cloud_download, color: REMOTE_COLOR);
        } else if (snapshot.data is String) {
          postIcon = Icon(Icons.open_in_new, color: LOCAL_COLOR);
        }
        return postIcon;
      },
    );
  }
}

Future<List> getPostDetails(String forumId, String postId,
    [bool isLinkPost]) async {
  final postDetails = await rs.RsGxsForum.getForumContent(forumId, [postId]);
  if (isLinkPost) {
    models.PostMetadata postMetadata =
        models.PostMetadata.fromJsonString(postDetails[0]["mMeta"]["mMsgName"]);

    try {
      final realPostIds = postMetadata.referred.split(" ");
      final realPostDetails =
          await rs.RsGxsForum.getForumContent(realPostIds[0], [realPostIds[1]]);
      return realPostDetails;
    } catch (e) {
      print("Could not retrieve original post $e");
      return [];
    }
  }
  return postDetails;
}
