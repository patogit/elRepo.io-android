part of ui;

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: FULL_PAGE_BACKGROUND_COLOR,
      appBar: AppBar(
        title: Text(IntlMsg.of(context).signUp),
      ),
      body: new Container(
        margin: defaultMargin(),
        child: new ListView(children: <Widget>[new SignUpForm()]),
      ),
    );
  }
}

class SignUpForm extends StatefulWidget {
  SignUpForm({Key key}) : super(key: key);

  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    String locationName;
    String password;
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            decoration: InputDecoration(
              labelText: IntlMsg.of(context).username,
            ),
            validator: (value) {
              if (value.isEmpty) {
                return IntlMsg.of(context).usernameCannotBeEmpty;
              }
              return null;
            },
            onSaved: (val) => locationName = val,
          ),
          TextFormField(
            decoration: InputDecoration(labelText: IntlMsg.of(context).password),
            validator: (value) {
              if (value.isEmpty) {
                return IntlMsg.of(context).passwordCannotBeEmpty;
              }
              return null;
            },
            onSaved: (val) => password = val,
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 5.0),
              child: RaisedButton(
                onPressed: () async {
                  // Validate will return true if the form is valid, or false if
                  // the form is invalid.
                  final form = _formKey.currentState;
                  if (form.validate()) {
                    form.save();
                    print('creating account for $locationName');
                    showLoadingDialog(context,
                        IntlMsg.of(context).creatingAccount
                        );
                    final location = await rs.RsLoginHelper.createLocation(
                        locationName, password);
                    print("Location: $location");
                    await rs.RsLoginHelper.login(location, password);
                    await rs.befriendTier1(hostname: "emiliocanzi");
                    await rs.befriendTier1(hostname: "micheleangiolillo");
                    await subscribeToForums();
                    Navigator.pop(context); //pop dialog
                    Navigator.pushReplacementNamed(context, '/main');
                  }
                },
                child: Text(IntlMsg.of(context).createAccount),
                color: Colors.purple[600],
                textColor: Colors.white,
              ),
            ),
          )
        ],
      ),
    );
  }
}
