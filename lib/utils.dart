import 'dart:io' as Io;
import 'package:permission_handler/permission_handler.dart';
import 'package:ext_storage/ext_storage.dart';

import "retroshare.dart" as rs;
import 'constants.dart';
import 'models.dart' as models;

List<String> findHashTags(String lookupString) {
  RegExp exp = new RegExp(r"(#(?:[^\x00-\x7F]|\w)+)");
  Iterable<RegExpMatch> matches = exp.allMatches(lookupString);
  List<String> hasTagsList = [];
  if (matches.isNotEmpty) {
    hasTagsList = matches.map((x) => x[0].substring(1)).toList();
  }
  print("Found tags: $hasTagsList");
  return hasTagsList;
}

List forumTypes = ["CONTENT", "TAG", "USER", "BOOKMARKS"];

Future<String> findOrCreateRepoForum(
    [String forumType = "CONTENT", String label = ""]) async {
  if (!forumTypes.contains(forumType)) {
    throw Exception("invalid forum type");
  }
  if (label != "") {
    label = "_$label";
  }
  String forumName = "$FORUM_PREPEND${API_VERSION}_$forumType$label";
  print("Find or create $forumName");
  var result = await rs.RsGxsForum.getForumsSummaries();
  var elRepoForums = result.where((i) => i["mGroupName"] == forumName).toList();
  var forumId;
  if (elRepoForums.length > 0) {
    print("Found existing elRepo.io forums");
    forumId = elRepoForums[0]["mGroupId"];
  } else {
    forumId = await rs.RsGxsForum.createForumV2(forumName);
    print("No elRepo.io forums found, creating: $forumName");
    rs.RsGxsForum.subscribeToForum(forumId, true);
  }
  return forumId;
}

Future<List> getPostHeaders(
    [String forumType = "CONTENT", String label = ""]) async {
  if (!forumTypes.contains(forumType)) {
    throw Exception("invalid forum type");
  }
  if (label != "") {
    label = "_$label";
  }
  final allForums = await rs.RsGxsForum.getForumsSummaries();
  final allForumNames = allForums.map((i) => i["mGroupName"]).toList();
  print("All known forum names: $allForumNames");
  final elRepoForums = allForums
      .where((i) =>
          i["mGroupName"] == "$FORUM_PREPEND${API_VERSION}_$forumType$label")
      .toList();
  Map forum;
  List allMessages = [];
  for (forum in elRepoForums) {
    print(
        'Found elRepo forum: ${forum["mGroupName"]} / id: ${forum["mGroupId"]}');
// TODO(nicoechaniz): correctly filter depending on forum["mSubscribeFlags"] values
    rs.RsGxsForum.subscribeToForum(forum["mGroupId"], true);
    var messages = await rs.RsGxsForum.getForumMsgMetaData(forum["mGroupId"]);
    allMessages.addAll(messages);
  }
  return allMessages;
}

Future<bool> createAppDir() async {
  print("Creating app dir");
  final externalDir = await ExtStorage.getExternalStoragePublicDirectory(
      ExtStorage.DIRECTORY_DOWNLOADS);
  final appDir = '$externalDir/$APP_DIR';

  bool granted = false;
  PermissionStatus permission =
      await PermissionHandler().checkPermissionStatus(PermissionGroup.storage);
  if (permission != PermissionStatus.granted) {
    Map<PermissionGroup, PermissionStatus> permissions =
        await PermissionHandler().requestPermissions([PermissionGroup.storage]);
    if (permissions[PermissionGroup.storage] == PermissionStatus.granted) {
      granted = true;
    }
  } else {
    granted = true;
  }
  if (granted) {
    var res = await createExternalDir(appDir);
    print("Setting RetroShare Download directory");
    return true;
  } else
    return false;
}

Future<bool> createExternalDir(String path) async {
  try {
    var dir = Io.Directory(path);
    bool dirExists = await dir.exists();
    if (!dirExists) {
      print("Creating directory: $path");
      await dir.create(recursive: true);
    }
  } catch (e) {
    print("Error creating directory. $e");
    return false;
  }
  return true;
}

getRealPostId(jsonMetadata) {
//  print('Link Post : $jsonMetadata');
  final linkPostMetadata = models.PostMetadata.fromJsonString(jsonMetadata);

  try {
    final realPostId = linkPostMetadata.referred.split(" ")[1];
    print("Real post id: $realPostId");
    return realPostId;
  } catch (error) {
    print("Could not retrieve original post: $error");
    return null;
  }
}

Future<List<dynamic>> getBookmarks() async {
  List bookmarkedIds;
  final allForums = await rs.RsGxsForum.getForumsSummaries();
  print("Fetching Bookmarks");
  final bookmarkForumName =
      "$FORUM_PREPEND${API_VERSION}_BOOKMARKS_${rs.authIdentityId}";
  final bookmarksForum =
      allForums.where((i) => i["mGroupName"] == bookmarkForumName).toList();
  if (bookmarksForum.length > 0) {
    final bookmarkMessages =
        await rs.RsGxsForum.getForumMsgMetaData(bookmarksForum[0]["mGroupId"]);
    bookmarkedIds = bookmarkMessages
        .map((item) => getRealPostId(item["mMsgName"]))
        .toList();
  }
  return bookmarkedIds;
}
