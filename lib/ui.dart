library ui;

import 'package:flutter/material.dart';
import 'dart:async';
import 'retroshare.dart' as rs;
import 'dart:convert';
import 'utils.dart' as utils;
import 'package:file_picker/file_picker.dart';
import 'models.dart' as models;
import 'package:path/path.dart' as path;
import 'package:ext_storage/ext_storage.dart';
import 'package:mime/mime.dart';
import 'constants.dart';
import 'package:open_file/open_file.dart';
import 'dart:io' as Io;
import 'intl.dart';

part "ui/browse.dart";
part "ui/postdetail.dart";
part "ui/publish.dart";
part "ui/login.dart";
part "ui/signup.dart";
part "ui/status.dart";
part "ui/settings.dart";
part "ui/tags.dart";
part "ui/browsetag.dart";
part "ui/bookmarks.dart";

class PostArguments {
  final String forumId;
  final String postId;
  final bool isLinkPost;
  PostArguments(this.forumId, this.postId, [this.isLinkPost]);
}

class TagArguments {
  final String tagName;
  TagArguments(this.tagName);
}

Future<dynamic> subscribeToForums() async {
  final allForums = await rs.RsGxsForum.getForumsSummaries();
  final elRepoForums = allForums
      .where((i) => i["mGroupName"] == "elRepo_${API_VERSION}_")
      .toList();
  print("Found elRepo.io forums $elRepoForums");
  Map forum;
  for (forum in elRepoForums) {
// TODO(nicoechaniz): correctly filter depending on forum["mSubscribeFlags"] values
    rs.RsGxsForum.subscribeToForum(forum["mGroupId"], true);
  }
}

Widget loadingBox([String loadingMsg = 'Loading ...']) {
  return Center(
    child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            child: CircularProgressIndicator(),
            width: 60,
            height: 60,
          ),
          new Padding(
            padding: EdgeInsets.only(top: 15),
            child: Text(loadingMsg),
          )
        ]),
  );
}

Future showLoadingDialog(context, [String loadingMsg = 'Loading ...']) {
  return showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return SimpleDialog(
        children: <Widget>[
          Center(
              child: Column(children: [
            CircularProgressIndicator(),
            SizedBox(
              height: 10,
            ),
            Text(loadingMsg, style: TextStyle(color: Colors.green[600]))
          ]))
        ],
      );
    },
  );
}

defaultMargin() {
  return EdgeInsets.all(15.0);
}

Future<bool> isBookmarked(bookmarkedIds, msgId) async {
  final bookmarked = await bookmarkedIds;
  if (bookmarked != null && bookmarked.contains(msgId))
    return true;
  else
    return false;
}

Widget getPostIcon(String contentType, Future<List> bookmarkedIds, msgId) {
  Icon postIcon;
  return FutureBuilder(
    future: isBookmarked(bookmarkedIds, msgId),
    builder: (context, snapshot) {
      if (snapshot.hasError) {
        print('Error: ${snapshot.error}');
        return Center(
          child: Text(IntlMsg.of(context).cannotLoadContent),
        );
      } else if (snapshot.hasData == false) {
        return Icon(Icons.hourglass_empty, color: NEUTRAL_COLOR);
      }
      switch (snapshot.data) {
        case (true):
          {
            postIcon =
                Icon(getContentTypeIcon(contentType), color: LOCAL_COLOR);
            break;
          }
        case (false):
          postIcon = Icon(getContentTypeIcon(contentType), color: REMOTE_COLOR);
      }
      return postIcon;
    },
  );
}

Widget postTeasersWidget(Future<List> postsHeaders, {bool linkPosts = false}) {
  final bookmarkedIds = utils.getBookmarks();

  return FutureBuilder(
    future: postsHeaders,
    builder: (context, snapshot) {
      if (snapshot.hasError) {
        print('Error: ${snapshot.error}');
        return Center(
          child: Text(IntlMsg.of(context).cannotLoadContent),
        );
      } else if (snapshot.hasData == false || snapshot.data.length == 0) {
        return loadingBox();
      } else {
        return ListView.builder(
          itemCount: snapshot.data.length,
          itemBuilder: (context, index) {
            final data = snapshot.data[index];
            models.PostMetadata postMetadata =
                models.PostMetadata.fromJsonString(data["mMsgName"]);

            return Card(
              elevation: 1.0,
              margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
              child: Container(
                decoration: BoxDecoration(color: Colors.white),
                child: ListTile(
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                    leading: Container(
                      padding: EdgeInsets.only(right: 12.0),
                      decoration: new BoxDecoration(
                          border: new Border(
                              right: new BorderSide(
                                  width: 1.0, color: Colors.green[200]))),
/*
                      child: Icon(getContentTypeIcon(postMetadata.contentType),
                          color: getStatusColor(bookmarkedIds, data["mMsgId"])),
*/
                      child: getPostIcon(postMetadata.contentType,
                          bookmarkedIds, data["mMsgId"]),
                    ),
                    onTap: () {
                      // if it is a linkPost, we need to redirect to the CONTENT forum post on click.
                      Navigator.pushNamed(context, "/postdetails",
                          arguments: PostArguments(
                              data["mGroupId"], data["mMsgId"], linkPosts));
                    },
                    title: Text(postMetadata.title),
                    subtitle: Text(postMetadata.summary != null
                        ? postMetadata.summary
                        : "")),
              ),
            );
          },
        );
      }
    },
  );
}

String getContentTypeFromPath(String filePath) {
  final fileName = path.basename(filePath);
  final fileType = lookupMimeType(fileName);
  var fileRootType = "";
  if (fileType is String) {
    fileRootType = fileType.split("/")[0];
  }
  if (contentTypeIcons.containsKey(fileRootType)) {
    return fileRootType;
  } else
    return ContentTypes.text;
}

IconData getContentTypeIcon(String contentType) {
  if (contentTypeIcons.containsKey(contentType)) {
    return contentTypeIcons[contentType];
  } else {
    // the default icon is the one we use for no payload
    return contentTypeIcons[ContentTypes.text];
  }
}

Map<String, IconData> contentTypeIcons = {
  ContentTypes.image: Icons.image,
  ContentTypes.audio: Icons.audiotrack,
  ContentTypes.video: Icons.local_movies,
  ContentTypes.document: Icons.insert_drive_file,
  ContentTypes.file: Icons.attach_file,
  ContentTypes.text: Icons.format_align_left
};
